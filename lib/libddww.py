""" ctypes bindings for libddww """
import numpy.ctypeslib
import ctypes
from ctypes import c_int16, c_uint16, c_int32, c_uint32, c_int64, c_uint64, c_float, c_double, c_char_p, c_void_p, POINTER
import os
import copy

#__libddww__ = numpy.ctypeslib.load_library('libddww8', "/afs/ipp-garching.mpg.de/aug/ads/lib64/%s" % os.environ['SYS'])
ddww_dir = '/afs/ipp/aug/ads/lib64/@sys'
ddww_dir = '/shares/software/aug-dv/moduledata/ads/Linux-generic-x86_64/lib64'
if not os.path.isdir(ddww_dir):
    ddww_dir = '/afs/ipp/aug/ads/lib64/amd64_sles11'
__libddww__ = numpy.ctypeslib.load_library('libddww8', ddww_dir)

c_int16_p = POINTER(c_int16)
c_uint16_p = POINTER(c_uint16)
c_int32_p = POINTER(c_int32)
c_uint32_p = POINTER(c_uint32)
c_int64_p = POINTER(c_int64)
c_uint64_p = POINTER(c_uint64)
c_float_p = POINTER(c_float)
c_double_p = POINTER(c_double)


def function(argtypes, restype = c_int32):
    return {'argtypes':argtypes, 'restype':restype}

functions = {
    'ddagroup':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, c_void_p, c_uint32_p]),
    'ddainfo':      function([c_int32_p, c_int32, c_char_p, numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32, shape=(3)), numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32, shape=(3)), c_int32_p]),
    'ddainfo2':     function([c_int32_p, c_int32, c_char_p, c_uint32_p, c_uint32_p, c_int32_p, c_char_p, c_int32_p]),
    'ddccsgnl':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p, c_int32_p, c_char_p]),
    'ddccsgrp':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p, c_int32_p, c_char_p]),
    'ddccxsig':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32_p, c_uint32, c_uint32, c_void_p, c_uint32_p, c_int32_p, c_char_p]),
    'ddclose':      function([c_int32_p, c_int32]),
    'ddcsgnl':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p, c_int32_p, c_char_p]),
    'ddcsgrp':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p, c_int32_p, c_char_p]),
    'ddcxsig':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p, c_int32_p, c_char_p]),
    'ddcshotnr':    function([c_char_p, c_char_p, c_uint32, c_uint32_p]),
    'dddelay':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_void_p]),
    'dddim':        function([c_int32_p, c_int32, c_char_p]),
    'ddevent':      function([c_int32_p, c_int32, c_char_p, c_int32, c_void_p, c_uint32, c_void_p, c_float_p, c_uint64]),
    'ddflist':      function([c_int32_p, c_int32, c_uint32, c_int32_p, c_int32_p]),
#    'ddgetaug':function(int32_t argc, void *argv[]);
    'dddgetsgr':    function([c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_double, c_double, c_char_p, c_double_p, c_double_p, c_uint32_p, c_uint32_p, c_uint32, c_char_p]),
    'dddgetsig':    function([c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_double, c_double, c_char_p, c_double_p, c_double_p, c_uint32_p, c_uint32, c_char_p]),

    'ddgetsgr':     function([c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_float, c_float, c_char_p, c_float_p, c_float_p, c_uint32_p, c_uint32_p, c_uint32, c_char_p]),
    'ddgetsig':     function([c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_float, c_float, c_char_p, c_float_p, c_float_p, c_uint32_p, c_uint32, c_char_p]),
    'ddlastshotnr': function([c_int32_p, c_uint32_p]),
    'ddlnames':     function([c_int32_p, c_int32, c_char_p, c_int32_p, c_char_p]),
    'ddmapinfo':    function([c_int32_p, c_int32, c_char_p, numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.int32), c_char_p, c_int32_p]),
    'ddobjdata':    function([c_int32_p, c_int32, c_char_p, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p]),
    'ddobjhdr':     function([c_int32_p, c_int32, c_char_p, numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.int32, shape=(26)), c_char_p]),
    'ddobjname':    function([c_int32_p, c_int32, c_int32, c_char_p]),
    #'ddobjval':     function([c_int32_p, c_int32, c_char_p, c_char_p, c_int32_p]),
    'ddobjval':     function([c_int32_p, c_int32, c_char_p, c_char_p, c_void_p]),
    'ddopen':       function([c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_int32_p, c_char_p]),
    'ddparm':       function([c_int32_p, c_int32, c_char_p, c_char_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_int32_p]),

#void   ddphys_ (int32_t *ier, int32_t *ref, char *name, uint32_t *type, uint32_t *vl, float *pdat, float *fmin, float *fmax, char *physdim, uint64_t lname, uint64_t lphysdim);

    'ddprinfo':     function([c_int32_p, c_int32, c_char_p, c_int32_p, c_char_p, numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32), numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint16), numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.int32)]),
    'dd_prinfo':    function([c_int32_p, c_int32, c_char_p, c_char_p, c_uint32_p, c_uint16_p]),
    'ddqget':       function([c_int32_p, c_int32, c_char_p, c_int32_p, c_uint32_p, numpy.ctypeslib.ndpointer(dtype=numpy.int32)]),
    'ddqinfo':      function([c_int32_p, c_int32, c_char_p, c_int32_p, numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32, shape=(3)), c_uint32_p]),
    'ddsgroup':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p]),
    #'ddshot_prefetch':function([c_int32_p, c_char_p, c_char_p, c_uint32_p, c_int32, c_uint32, c_int32]),
    'ddsignal':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p]),
    'ddsinfo':      function([c_int32_p, c_int32, c_char_p, c_uint32_p, c_char_p, numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32, shape=(4))]),
    'ddtbase':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p]),
    'ddtindex':     function([c_int32_p, c_int32, c_char_p, c_float, c_float, c_uint32_p, c_uint32_p]),
    'dddtindex':    function([c_int32_p, c_int32, c_char_p, c_double, c_double, c_uint32_p, c_uint32_p]),
    'ddtindexneu':  function([c_int32_p, c_int32, c_char_p, c_float, c_float, c_uint32_p, c_uint32_p]),
    'dddtindexneu': function([c_int32_p, c_int32, c_char_p, c_double, c_double, c_uint32_p, c_uint32_p]),

    'ddtmout':      function([c_int32_p, c_int32]),
    'ddtrange':     function([c_int32_p, c_int32, c_char_p, c_float_p, c_float_p, c_uint32_p, c_uint32_p]),
    'dddtrange':    function([c_int32_p, c_int32, c_char_p, c_double_p, c_double_p, c_uint32_p, c_uint32_p]),
    'dduprmd':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_float_p, c_int32_p, c_int32_p, c_int32_p, c_uint32_p]),
    'dduprot':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_float_p, c_int32_p, c_uint32_p, c_uint32_p]),
    'ddwait':       function([c_int32_p, c_uint32_p]),
    'ddwaitt':      function([c_int32_p, c_uint32_p, c_int32]),
    'ddxtrsignal':  function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p]),
    'wwainsert':    function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), numpy.ctypeslib.ndpointer(dtype=numpy.uint32, ndim=1, shape=(3))]),
    'wwclear':      function([c_int32_p, c_int32, c_char_p]),
    'wwclose':      function([c_int32_p, c_int32, c_char_p, c_char_p]),
    'wwinsert':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32, numpy.ctypeslib.ndpointer(dtype=numpy.uint32, ndim=1, shape=(3))]),
    'wwoinfo':      function([c_int32_p, c_int32, c_char_p, c_uint32_p, numpy.ctypeslib.ndpointer(dtype=numpy.uint16), c_uint32_p, numpy.ctypeslib.ndpointer(dtype=numpy.int32), numpy.ctypeslib.ndpointer(dtype=numpy.uint32)]),
    'wwonline':     function([c_int32_p, c_char_p, c_char_p, c_int32]),
    'wwopen':       function([c_int32_p, c_char_p, c_char_p, c_uint32, c_char_p, c_int32_p, c_int32_p, c_char_p]),
    'wwparm':       function([c_int32_p, c_int32, c_char_p, c_char_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32]),
    'wwpred':       function([c_int32_p, c_int32, c_char_p, c_char_p, c_uint32, c_int32]),
    'wwqset':       function([c_int32_p, c_int32, c_char_p, c_int16, c_uint32, c_int32]),
    'wwsfile':      function([c_int32_p, c_int32, c_int32]),
    'wwsignal':     function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32]),
    'wwtbase':      function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, numpy.ctypeslib.ndpointer(), c_uint32]),
    'wwtext':       function([c_int32_p, c_int32, c_char_p, c_char_p]),

    'xxcom':        function([c_int32]),
    'xxerrprt':     function([c_int32, c_char_p, c_int32, c_uint32, c_char_p]),
    'xxerror':      function([c_int32, c_uint32, c_char_p]),
    'xxnote':       function([c_int32]),
    'xxsev':        function([c_int32]),
    'xxwarn':       function([c_int32])


#void   xxdiaglog_ (char *msg, int32_t wohin);
#
#
#/* definitions for the current DDB Fortran Library */
#	int32_t    ddb_delete_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_time_out, uint64_t lexper, uint64_t  ldiag);
#	int32_t    ddb_do_send (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_val1, int32_t *p_val2,
#				            int32_t *p_val3, int32_t *p_time_out, char *buffer, int32_t *no_lines, int32_t request, uint64_t lexper, uint64_t ldiag, uint64_t  lline);
#	int32_t    ddb_insert_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_time_out, char *buffer,
#				       int32_t *no_lines, uint64_t lexper, uint64_t ldiag, uint64_t  lline);
#	int32_t    ddb_list_ppred_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_time_out,
#		          char *buffer, int32_t *no_lines, uint64_t lexper, uint64_t ldiag, uint64_t  lline);
#	int32_t    ddb_list_pred_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_time_out,
#		         char *no_lines, int32_t *l_text, uint64_t lexper, uint64_t ldiag, uint64_t  lline);
#	int32_t    ddb_list_succ_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_time_out,
#		          char *buffer, int32_t *no_lines, uint64_t lexper, uint64_t ldiag, uint64_t  lline);
#	int32_t    ddb_mod_quality_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *p_quality,
#					         int32_t *p_time_out, uint64_t lexper, uint64_t ldiag);
#	int32_t    ddb_mod_status_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *status,
#					        int32_t *p_time_out, uint64_t lexper, uint64_t ldiag);
#	int32_t    ddb_query_file_ (int32_t *error, char *exper, char *diag, uint32_t *shot, int32_t *edition, int32_t *status,
#					        int32_t *p_verification, int32_t *p_quality, int32_t *p_time_out, char *buffer, int32_t *no_lines,
#					        uint64_t lexper, uint64_t ldiag, uint64_t  lline);
#	int32_t    ddb_query_user_ (int32_t *error, char *exper, char *diag, char *p_user, char *p_capability, int32_t *p_time_out, uint64_t lexper, uint64_t ldiag, uint64_t luser, uint64_t lcap);
#	void   ddbgetuid_num_ (int32_t *error, int32_t *num, char *username, uint64_t lusername);

        }

for func in functions:
    try:
        vars()[func] = __libddww__[func]
        vars()[func].argtypes = functions[func]['argtypes']
        vars()[func].restype = functions[func]['restype']
    except Exception as Error: # modified git 07.05.19 for python3 compatibility
        print(Error)

